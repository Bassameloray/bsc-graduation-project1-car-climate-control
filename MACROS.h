/********************************************** The Macros Header File *****************************************************/
#ifndef MACROS_H_																										/* If The Headers Are not defined yet it will be defined, if it was already defined it won't be defined again */
#define MACROS_H_																										/* */
/*********************************************** Some Macros Functions *****************************************************/
#define F_CPU 8000000UL
#define SET_BIT(REGISTER,BIT_NUM) (REGISTER = REGISTER | (1<<BIT_NUM))													/* Sets A Certain Port To Output (1) */
#define CLEAR_BIT(REGISTER,BIT_NUM) (REGISTER = REGISTER & ~(1<<BIT_NUM))												/* Resets The Port To Input (0) */
#define CLEAR_ALL_BITS(REGISTER) (REGISTER = REGISTER & 0)																/* Resets All Bits To Inputs */
#define TOGGLE_BIT(REGISTER,BIT_NUM) (REGISTER ^= REGISTER | (1<<BIT_NUM))												/* Toggles A Bit. If The Bit is Output It Will Become Input and vice versa */
#define IS_BIT_SET(REGISTER,BIT_NUM) ((REGISTER & (1<<BIT_NUM))>>BIT_NUM)												/* Reads A Certain Bit State, If Output It Will Return '1', If Input It Will Return '0'. PS: Example: "State = Is_Bit_Set(PORTD,LED2);" Then Check The Value Of 'State' */
#define IS_BIT_CLEAR(REGISTER,BIT_NUM) ((!(REGISTER & (1<<BIT_NUM)))>>BIT_NUM)											/* Reads A Certain Bit State, If Output It Will Return '0', If Input It Will Return '1'. */
#define ROR(REF,BIT_NUM) (REG = (REG>>BIT_NUM) | (REG<<(8-BIT_NUM)))													/* Rotates The Whole Byte's Bits To The Right By A Certain Rotation Steps */
#define ROL(REF,BIT_NUM) (REG = (REG<<BIT_NUM) | (REG>>(8-BIT_NUM)))													/* Rotates The Whole Byte's Bits To The Left By A Certain Rotation Steps */
/************************************************ Magic Numbers' Macros ****************************************************/
#define LED2	7																										/* LED2 Shift*/
#define LED1	6																										/* LED1 Shift*/
#define LED0	5																										/* LED0 Shift*/
#define BUZZER  4																										/* Buzzer Shift*/
#define RELAY   3																										/* Relay Shift*/
#define BUTTON2	2																										/* Button2 Shift*/
#define BUTTON1	1																										/* Button1 Shift*/
#define BUTTON0	0																										/* Button0 Shift*/
#define OUTPUT	1																										/* Set The Pin To Output*/
#define INPUT	0																										/* Set The Pin To Input*/
#define HIGH	1																										/* ON */
#define ON  1
#define FOREVER 1
#define INITIALIZATION_ZERO 0
#define TEMP_INITIAL_VALUE 18
#define INITIAL_SPEED_LEVEL 1
#define SPEED_LEVEL_ONE 1
#define SPEED_LEVEL_TWO 2
#define SPEED_LEVEL_THREE 3
#define LOW		0																										/* OFF */
#define PORT0	0																										/* */
#define PORT1	1																										/* */
#define PORT2	2																										/* */
#define PORT3	3																										/* */
#define PORT4	4																										/* */
#define PORT5	5																										/* */
#define PORT6	6																										/* */
#define PORT7	7																										/* */
#define BIT1	1																										/* */
#define BIT2	2																										/* */
#define BIT3	3																										/* */
#define BIT4	4																										/* */
#define BIT5	5																										/* */
#define BIT6	6																										/* */
#define BIT7	7																										/* */
#define BIT8	8																										/* */
/*********************************************** LCD Commands & Magic Numbers **********************************************/
//#define MODE_8_BITS 0x38																								/* */
//#define CLEAR_LCD 0x01																									/* */
//#define CURSOR_ON 0x0e																									/* */
//#define FIRST_ROW 1																										/* */
//#define SECOND_ROW 2																									/* */
//#define ZERO_ROW 0																										/* */
//#define MODE_4_BITS 0x28																								/* */
//#define RETURN_CURSOR_TO_HOME 0x02																									/* */
//#define FIRST_ROW_CURSOR_HOME 0x80																						/* */
//#define SECOND_ROW_CURSOR_HOME 0xC0																						/* */
//#define COLUMN8 8																										/* */
//#define COLUMN0 0																										/* */
/******************************************** Some LCD Commands add When Needed ********************************************/
/***** Make Sure To Add 0x Before the Command And To Name The Macro A Name That Indicates The Function Of The Command ******/
/*Command	Function*/																									/* */
/*0F		LCD ON, Cursor ON, Cursor blinking ON*/																		/* */
/*01		Clear screen*/																								/* */
/*02		Return home*/																								/* */
/*04		Decrement cursor*/																							/* */
/*06		Increment cursor*/																							/* */
/*0E		Display ON ,Cursor blinking OFF*/																			/* */
/*80		Force cursor to the beginning of  1st line*/																/* */
/*C0		Force cursor to the beginning of 2nd line*/																	/* */
/*38		Use 2 lines and 5�7 matrix*/																				/* */
/*83		Cursor line 1 position 3*/																					/* */
/*3C		Activate second line*/																						/* */
/*08		Display OFF, Cursor OFF*/																					/* */
/*C1		Jump to second line, position1*/																			/* */
/*OC		Display ON, Cursor OFF*/																					/* */
/*C1		Jump to second line, position1*/																			/* */
/*C2		Jump to second line, position2*/																			/* */
/*************************************************** End Of Text ***********************************************************/
#define US_TRIGGER 0
#endif
