/* This Project is meant to control the Climate inside the car by Controlling how much */   /* */
/* Power is given to The Piltiers/Fans with the help of different modes/speeds */           /* */
/* Generally, the Fans' speed would be of THREE Speeds (Low - Medium - High) */             /* */
/* The Piltiers on the other hand would either be ON or OFF depending of a Tempreture and *//* */
/* Humidity Sensor 'HDT11' */                                                               /* */
/* The Microcontroller used is 'ATMega32'*/                                                 /* */
//#include <stdio.h>                                                                        /* */
//#include <stdlib.h>                                                                       /* */
//#include <stdint.h>                                                                       /* */
//#include <inttypes.h>                                                                     /* */
#include <avr/io.h>																			/* Main Input/Output Header */
#include <util/delay.h>																		/* Delay Header */
#define F_CPU 8000000UL                                                                     /* */
#include <avr/interrupt.h>                                                                  /* */
#include "MACROS.h"                                                                         /* */
#include "DIO.h"                                                                            /* */
/***********************************************************************************************/
                                                                                            /* Global Variables Declaration */
//uint8_t Seven_Seg_Input=00011001;                                                         /* Controls The Numbers Printed on the 7 Semgent */
                                                                                            /* */
uint8_t Speed_Level = INITIAL_SPEED_LEVEL;                                                  /* Controls the fan speed */
uint8_t Desired_Temp=TEMP_INITIAL_VALUE;                                                    /* Desired Tempreture Control */
uint8_t Fan_Or_Piltiers=INITIALIZATION_ZERO;                                                /* Choosing the PIN Piltiers-pin, FAN_FIRST_LEVEL, FAN_SECOND_LEVEL, FAN_THIRD_LEVEL */
uint8_t Piltiers_Status=HIGH;                                                               /* Controls The Piltiers Status ON/OFF */
uint8_t On_Or_Off=ON;                                                                       /* Determine The Status ON or OFF */
uint8_t First_Humidity=INITIALIZATION_ZERO, Second_Humidity=INITIALIZATION_ZERO;            /* Two Humidity Variables for two segments */
                                                                                            /* If the Tempreture Is below 18 Already The System Will Not Work So I Don't need signed Variables */
                                                                                            /* For The Tempreture so I will use and unsigned Variables */
uint8_t First_Temp=INITIALIZATION_ZERO, Second_Temp=INITIALIZATION_ZERO;                    /* Two Tempreture Variables For Two Segments */
uint8_t CheckSum=INITIALIZATION_ZERO;                                                       /* */
#include "Fans.h"                                                                           /* */
#include "Piltiers.h"                                                                       /* */
#include "Sensor.h"                                                                         /* */
#include "7Segment.h"                                                                       /* */
    																					    /* */
 /*************************** Turn Interrupt ON, I need 4 Interrupts *********************************/
                                                                                            //I can not use the external interrupt because I have only 3 external interrupts
                                                                                            //think of something else
                                                                                            //Look! The Tempreture and the fan spped are not actually Critical
                                                                                            //you can make them u/sing switch and wires so you don't have to control them using the software
                                                                                            //put them inside the loop
                                                                                            //If I put the control push buttons inside the loop I will have to keep the button pressed until
                                                                                            //the loop reads it which may take more than one second.
                                                                                            //why don't you make three buttons using the interrupts and only one inside the loop?
                                                                                            //make the Fan Down Button inside the loop and the rest outside the loop
void VInterrupt_Init()                                                                      /* */
{                                                                                           /* */
                                                                                            /* Enablinig Interrupts 0,1,2 */
  GICR |= (HIGH<<INT0);                                                                     /* */
  GICR |= (HIGH<<INT1);                                                                     /* */
  GICR |= (HIGH<<INT2);                                                                     /* */
                                                                                            /* Set The Edge That fires The Interrupt */
                                                                                            /*Making sure All MCUCR Bits are ZEROS */
  MCUCR = LOW;                                                                              /* */
                                                                                            /*Interrupt INT0 Falling Edge ISC01=1 ISC00=0 */
  MCUCR = (HIGH<<ISC01);                                                                    /* */
                                                                                            /*Interrupt INT1 Falling Edge ISC11=1 ISC10=0 */
  MCUCR |= (HIGH<<ISC11);                                                                   /* */
                                                                                            /* What about INT2?*/
                                                                                            /* Making Sure ALL MCUCSR BITS are ZERO */
  MCUCSR =LOW;                                                                              /* */
                                                                                            /* Interrpt 2 set to falling edge ISC2=0 */
                                                                                            /* Optional */
  MCUCSR = (LOW<<ISC2);                                                                     /* */
  sei();                                                                                    /* */
}                                                                                           /* */
/***************************************** The Main Function ***********************************/
int main()                                                                                  /* */
  {                                                                                         /* */
    Fan_Init();                                                                             /* */
    Piltiers_Init();                                                                        /* */
    VInterrupt_Init();                                                                      /* */
    VSeven_Seg_Init();                                                                      /* */
    VSensor_Init();                                                                         /* */
    while(FOREVER)                                                                          /* */
	{																						/* */
    /* Make Sure The Values Are Within The Right Range */									/* */
    while( (Speed_Level >= SPEED_LEVEL_ONE) && (Speed_Level <= SPEED_LEVEL_THREE) )         /* */
    {																						/* */
    /*Checking If The Fan Speed Button Is Pressed */										/* */
//    if(  DIO_CReadPin( 'A' , PORT7) == 0)                                                 /* Write The Fan Speed Pull Down Level Pin */
//    {                                                                                     /* */
//      DIO_VSetPort_IO( 'B' , FAN_FIRST_LEVEL , 'L' );                                     /* */
//      DIO_VSetPort_IO( 'B' , FAN_SECOND_LEVEL , 'L' );                                    /* */
//      DIO_VSetPort_IO( 'B' , FAN_THIRD_LEVEL , 'L' );                                     /* */
//    Speed_Level--;                                                                        /* Making Sure The Fan Speed Level is not lower than 1 */
//    }                                                                                     /* */
                                                                                            /* Write THe Fan Speed On The 7-Segment */
    Seven_Seg_Fan_Write(Speed_Level);                                                       /* */
    Fan_Speed_Control(Speed_Level);                                                         /* */
                                                                                            /* FIRST STEP: Getting reading From the DHT11 Sensor */
		Request();		                                                                    /* send start pulse */
		Response();   			                                                            /* receive response */
		First_Humidity=Receive_data();	                                                    /* store first eight bit in I_RH */
		Second_Humidity=Receive_data();	                                                    /* store next eight bit in D_RH */
		First_Temp=Receive_data();                                                          /* store next eight bit in I_Temp */
                                                                                            /*Shouldn't This one be Second_Temp?*/
                                                                                            /*This was First_Temp*/
		Second_Temp=Receive_data();	                                                        /* store next eight bit in D_Temp */
		CheckSum=Receive_data();                                                            /* store next eight bit in CheckSum */
                                                                                            /* Checking The Checksum and making sure we got right readings */
		if (( First_Humidity + Second_Humidity + First_Temp + Second_Temp) == CheckSum)     /* If The Checksum Is Correct */
		{                                                                                   /* */
      if(Desired_Temp <= First_Temp )                                                       /* If The Tempreture is not the same Start/Keep Cooling */
      {                                                                                     /* */
                                                                                            /* Now Determine The Fans Speed That user Indicated */
                                                                                            /* Start The Fans with that speed and turn the Piltiers ON */
        Fan_Speed_Control(Fan_Or_Piltiers);                                                 /*Check the fan deired level and perform it  */
        Piltiers_Control(HIGH);                                                             /* Turn The Piltiers ON */
      }                                                                                     /* */
                                                                                            /* The Problem Here is that once the temo gets down by a single degree the system will work again*/
                                                                                            /* Which means it will be turned off and on a LOT */
      else if(Desired_Temp > First_Temp)                                                    /* */
      {                                                                                     /* */
                                                                                            /* Trn The Piltiers OFF as well as the Fans */
                                                                                            /*Should I turn the fan OFF?*/
        Fan_Speed_Control(Fan_Or_Piltiers);                                                 /* Turn The Fans Off */
        Piltiers_Control(LOW);                                                              /* Turn The Piltiers Off */
      }                                                                                     /* */
      else                                                                                  /* Misra-C Practice To handle Unexpected Behavior */
      {                                                                                     /* */
        ;                                                                                   /* */
      }                                                                                     /* */
                                                                                            /* Write The Temp. To The 7-Segment*/
      Seven_Seg_Temp_Write(Desired_Temp);                                                   /* */
                                                                                            /* Delay 10 ms (Optional)*/
		}                                                                                   /* */
                                                                                            /* Else If The Checksum does not match, do nothing Then Restart The Loop */
		else if((First_Humidity + Second_Humidity + First_Temp + Second_Temp) != CheckSum)  /* If an error has occured, Read again */
		{                                                                                   /* */
      ;                                                                                     /* */
		}                                                                                   /* */
                                                                                            /* Mesra C Practice */
    else                                                                                    /* */
    {                                                                                       /* */
      ;                                                                                     /* */
    }                                                                                       /* */
    Seven_Seg_Fan_Write(Speed_Level);                                                       /* */
    Fan_Speed_Control(Speed_Level);                                                         /* */
}                                                                                           /* */
/* If I used 'Else if' If someone kept the button pressed or the Button got Broken the system Might Not work */                                                                            
}                                                                                           /* */
/*************************************** End Of The Main Loop **********************************/
/**************************************** Interrupts Section ***********************************/
                                                                                            /* Define The 3 Interrupts for the Fans Speed Display and the Tempreture Display */
                                                                                            /* */
ISR(INT0_vect)                                                                              /* Interrupt 0 For The Tempreture Button Up */
{                                                                                           /* */
                                                                                            /* Tempreture up */
  Desired_Temp++;                                                                           /* */
                                                                                            /*Change 7 Segment Value */
   Seven_Seg_Temp_Write(Desired_Temp);                                                      /* */
}                                                                                           /* End Of Interrupt 0 */
                                                                                            /* */
                                                                                            /* */
ISR(INT1_vect)                                                                              /* Interrupt 1 For The Tempreture Down */
{                                                                                           /* */
                                                                                            /* Tempreture Down */
  Desired_Temp--;                                                                           /* */
                                                                                            /* Change Seven Segment Value */
  Seven_Seg_Temp_Write(Desired_Temp);                                                       /* */
}                                                                                           /* End Of Interrupt 1 */
                                                                                            /* */
                                                                                            /* */
ISR(INT2_vect)                                                                              /* Interrupt 2 For The Fan Speed Up */
{                                                                                           /* */
                                                                                            /* Fan Speed Up */
  Speed_Level++;																			/* */
  if(Speed_Level > SPEED_LEVEL_THREE)														/* */
  {																							/* */
    Speed_Level = SPEED_LEVEL_ONE;															/* */
  }																							/* */
  Seven_Seg_Fan_Write(Speed_Level);                                                         /* */
  Fan_Speed_Control(Speed_Level);                                                           /* */
}                                                                                           /* End Of Interrupt 2 */
/*********************************** End Of Interrupt Section **********************************/
 /* There might be a delay between the reading on the 7-Segment and the system real values due to using interrupts and a loop */
