#ifndef FANS_H_
#define FANS_H_
/***********************************************************************************/
#include <stdio.h>                                                              /* */
#define F_CPU 8000000UL                                                         /* */
#include <stdlib.h>                                                             /* */
#include <stdint.h>                                                             /* */
#include <inttypes.h>                                                           /* */
#include <avr/io.h>																								              /* Main Input/Output Header */
#include <util/delay.h>																								          /* Delay Header */
#include <avr/interrupt.h>                                                      /* */
#include "MACROS.h"                                                             /* */
#include "DIO.h"                                                                /* */
                                                                                /* */
#define FAN_OFF 0                                                               /* */
#define FAN_FIRST_LEVEL         5                                               /* PORTB6   For THe LOwe LEVEL    */
#define FAN_SECOND_LEVEL        6                                               /* PORTB7   FOR the medium level  */
#define FAN_THIRD_LEVEL         7                                               /* PORTB8   For THe HIGHT level   */
                                                                                /* */
void VFan_Spees_Decrease();                                                     /* */
void Fan_Init();                                                                /* */
void Fan_Speed_Control(uint8_t Speed_Level_2);                                  /* */
                                                                                /* */
#endif                                                                          /* */
/***********************************************************************************/
