/******************************************** Starting The '.h' File ***************************************************/
#ifndef DIO_H_																								                          		/* If The Headers Are not defined yet it will be defined, if it was already defined it won't be defined again */
#define DIO_H_																									                          	/* */
/******************************************* Including The Header Files ************************************************/
#include <stdio.h>
#define F_CPU 8000000UL
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <avr/io.h>																								                         	/* Main Input/Output Header */
#include <util/delay.h>																								                      /* Delay Header */
#include <avr/interrupt.h>
#include "MACROS.h"
/*LED_DIRECTION = 0xff;*/
/*LED_PORT = 0xff;*/																							                        /* Some Simple Macros */


/************************************************** Macros *************************************************************/

/************************************************ Prototyping **********************************************************/
void DIO_VSetPinDir(char PORT_LETTER,char PIN_NUMBER,char STATE);										   			/* DIO-VSetPinDir( (A,B,C Or D) , ( 0,1,2,3,4,5,6,7 ) , (ON Or OFF) ) */
void DIO_VSetAllPinDir(char PORT_LETTER,char STATE);														        		/* To Set All DDR-Port On Or Off */
void DIO_VSetPort_IO(char PORT_LETTER,char PIN_NUMBER,char STATE);													/* To Set The Port To Input Or Output */
char DIO_CReadPin(char PORT_LETTER,char PIN_NUMBER);													        			/* To Read The State Of A Certain Port */
/************************************************ End Of Flie **********************************************************/
#endif
