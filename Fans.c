/*********************************************************************************************/
#include <stdio.h>                                                                        /* */
#define F_CPU 8000000UL                                                                   /* */
#include <stdlib.h>                                                                       /* */
#include <stdint.h>                                                                       /* */
#include <inttypes.h>                                                                     /* */
#include <avr/io.h>																								                        /* Main Input/Output Header */
#include <util/delay.h>																								                    /* Delay Header */
#include <avr/interrupt.h>                                                                /* */
#include "MACROS.h"                                                                       /* */
#include "DIO.h"                                                                          /* */
#include "Fans.h"                                                                         /* */
                                                                                          /* */
void Fan_Init()                                                                           /* */
{                                                                                         /* */
  DIO_VSetPinDir( 'A' , PORT7 , 'L' );                                          /* Enable THE PORT As INPUT */
  DIO_VSetPort_IO( 'A' , PORT7 , 'L' );                                         /* Initiate The Fan With Level 1 (SLOW) */
//  DDRB |= (1<<PORT3);
  DIO_VSetPinDir( 'B' , FAN_FIRST_LEVEL , 'H' );                                          /* Enable THE PORT */
  DIO_VSetPinDir( 'B' , FAN_SECOND_LEVEL , 'H' );                                         /* Enable THE PORT */
  DIO_VSetPinDir( 'B' , FAN_THIRD_LEVEL , 'H' );                                          /* Enable THE PORT AS OUTPUT */
  DIO_VSetPort_IO( 'B' , FAN_FIRST_LEVEL , 'H' );                                         /* Initiate The Fan With Level 1 (SLOW) */
}                                                                                         /* */
                                                                                          /* */
void VFan_Speed_Decrease()
{
  /* Making Sure The Fan Speed Level is not higher than 3 */
}

void Fan_Speed_Control(uint8_t Speed_Level_2)                                             /* */
{                                                                                         /* */
  switch(Speed_Level_2)                                                                   /* */
  {                                                                                       /* */
    case 1 :                                                                              /* Make The Pin For Level 1 Output And Turn Off The Rest*/
    {                                                                                     /* */
      DIO_VSetPort_IO( 'B' , FAN_FIRST_LEVEL , 'H' );                                     /* */
      DIO_VSetPort_IO( 'B' , FAN_SECOND_LEVEL , 'L' );                                    /* */
      DIO_VSetPort_IO( 'B' , FAN_THIRD_LEVEL , 'L' );                                     /* */
      break;                                                                              /* */
    }                                                                                     /* */
    case 2:                                                                               /* Make The Pin For Level 2 Output And Turn Off The Rest */
    {                                                                                     /* */
      DIO_VSetPort_IO( 'B' , FAN_FIRST_LEVEL , 'L' );                                     /* */
      DIO_VSetPort_IO( 'B' , FAN_SECOND_LEVEL , 'H' );                                    /* */
      DIO_VSetPort_IO( 'B' , FAN_THIRD_LEVEL , 'L' );                                     /* */
      break;                                                                              /* */
    }                                                                                     /* */
    case 3:                                                                               /* Make The Pin For Level 3 Output And Turn Off The Rest*/
    {                                                                                     /* */
      DIO_VSetPort_IO( 'B' , FAN_FIRST_LEVEL , 'L' );                                     /* */
      DIO_VSetPort_IO( 'B' , FAN_SECOND_LEVEL , 'L' );                                    /* */
      DIO_VSetPort_IO( 'B' , FAN_THIRD_LEVEL , 'H' );                                     /* */
      Speed_Level_2 = 1;
      break;                                                                              /* */
    }                                                                             /* */
    default:
{
  Speed_Level_2 =1;
  DIO_VSetPort_IO( 'B' , FAN_FIRST_LEVEL , 'H' );                                     /* */
  DIO_VSetPort_IO( 'B' , FAN_SECOND_LEVEL , 'L' );                                    /* */
  DIO_VSetPort_IO( 'B' , FAN_THIRD_LEVEL , 'L' );                                     /* *
}
      break;                                                                              /* */
}                                                                                         /* */
}
}                                                                                         /* */
/*********************************************************************************************/
