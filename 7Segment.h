#ifndef SEVEN_SEGMENT_H_
#define SEVEN_SEGMENT_H_

#include <stdio.h>
#define F_CPU 8000000UL
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <avr/io.h>																								                         	/* Main Input/Output Header */
#include <util/delay.h>																								                      /* Delay Header */
#include <avr/interrupt.h>
#include "MACROS.h"
#include "DIO.h"
/*#define LED_DIRECTION (DDRC && DDRA)	*/	/* define LED Direction */
/*#define LED_PORT (PORTC && PORTA)	*/		/* define LED port */
/* Defining The SEVEN SEGMENT PORTS */
/* Turning DDRA ON*/

/*Do NOT forget to open the ports for the 7Segments and make the PORTS output
DDRx
PORTX*/
/* Macros For The SEVEN SEGMENT NUMBERING */
#define SEVEN_SEG_ZERO									0x0
#define SEVEN_SEG_ONE										0x1
#define SEVEN_SEG_TWO										0x2
#define SEVEN_SEG_THREE									0x3
#define SEVEN_SEG_FOUR									0x4
#define SEVEN_SEG_FIVE									0x5
#define SEVEN_SEG_SIX										0x6
#define SEVEN_SEG_SEVEN 								0x7
#define SEVEN_SEG_EIGHT 								0x8
#define SEVEN_SEG_NINE 									0x9
/* Since we woll be Controlling 1-Bytes ( 8 Bit ) ( TWO SEVEVEN SEGMENTS ) */
/* I Will Deal with them as totally Seperate Units Connected At The Same Register */
#define SEVEN_SEG_EIGHTEEN						 	0x18	/*1	8*/
#define SEVEN_SEG_NINETEEN							0x19	/*1	9*/
#define SEVEN_SEG_TWENTY 								0x20	/*2	0*/
#define SEVEN_SEG_TWENTY_ONE 						0x21	/*2	1*/
#define SEVEN_SEG_TWENTY_TWO 						0x22 	/*2	2*/
#define SEVEN_SEG_TWENTY_THREE 					0x23	/*2	3*/
#define SEVEN_SEG_TWENTY_FOUR 					0x24	/*2	4*/
#define SEVEN_SEG_TWENTY_FIVE 					0x25	/*2	5*/
#define SEVEN_SEG_TWENTY_SIX 						0x26	/*2	6*/
#define SEVEN_SEG_TWENTY_SEVEN 					0x27	/*2	7*/
#define SEVEN_SEG_TWENTY_EIGHT 					0x28	/*2	8*/
#define SEVEN_SEG_TWENTY_NINE 					0x29	/*2	9*/
#define SEVEN_SEG_THIRTY 								0x30	/*3	0*/
#define SEVEN_SEG_THIRTY_ONE 						0x31	/*3	1*/
#define SEVEN_SEG_THIRTY_TWO 						0x32	/*3	2*/
#define SEVEN_SEG_THIRTY_THREE 					0x33	/*3	3*/
#define SEVEN_SEG_THIRTY_FOUR 					0x34	/*3	4*/
#define SEVEN_SEG_THIRTY_FIVE 					0x35	/*3	5*/

void VSeven_Seg_Init();
void Seven_Seg_Temp_Write(uint16_t Desired_Temp_1);
void Seven_Seg_Fan_Write(uint16_t Speed_Level_1);

#endif
