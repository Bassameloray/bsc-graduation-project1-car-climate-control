#include <stdio.h>
#define F_CPU 8000000UL
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <avr/io.h>																								                         	/* Main Input/Output Header */
#include <util/delay.h>																								                      /* Delay Header */
#include <avr/interrupt.h>
#include "MACROS.h"
#include "DIO.h"
#include "7Segment.h"


void VSeven_Seg_Init()
{
	/**************** Enabling A Register And Ports *******************/
	DIO_VSetPinDir( 'A' , PORT0 , 'H' );		/* Enable Port 						*/
	DIO_VSetPinDir( 'A' , PORT1 , 'H' );		/* Enable Port 						*/
	DIO_VSetPinDir( 'A' , PORT2 , 'H' );		/* Enable Port 						*/
	DIO_VSetPinDir( 'A' , PORT3 , 'H' );		/* Enable Port 						*/
	DIO_VSetPort_IO( 'A' , PORT0 , 'h');				/* Make The Port Output 	*/
	DIO_VSetPort_IO( 'A' , PORT1 , 'h');				/* Make The Port Output 	*/
	DIO_VSetPort_IO( 'A' , PORT2 , 'h');				/* Make The Port Output 	*/
	DIO_VSetPort_IO( 'A' , PORT3 , 'h');				/* Make The Port Output 	*/
	/**************** Enabling C Register And Ports *******************/
	DIO_VSetAllPinDir( 'C' , 'h');					/* Enable All C-Register	*/
	DIO_VSetPort_IO( 'C' , PORT0 , 'h');				/* Make The Port Output 	*/
	DIO_VSetPort_IO( 'C' , PORT1 , 'h');				/* Make The Port Output 	*/
	DIO_VSetPort_IO( 'C' , PORT2 , 'h');				/* Make The Port Output 	*/
	DIO_VSetPort_IO( 'C' , PORT3 , 'h');				/* Make The Port Output 	*/
	DIO_VSetPort_IO( 'C' , PORT4 , 'h');				/* Make The Port Output 	*/
	DIO_VSetPort_IO( 'C' , PORT5 , 'h');				/* Make The Port Output 	*/
	DIO_VSetPort_IO( 'C' , PORT6 , 'h');				/* Make The Port Output 	*/
	DIO_VSetPort_IO( 'C' , PORT7 , 'h');				/* Make The Port Output 	*/
	/******************************************************************/
}
																												/* I need to add Parameters to determine which 7 Segment will be controlled */
void Seven_Seg_Temp_Write(uint16_t Desired_Temp_1)									/* This data should be the readings from the Temp Sensor */
																												/* You have to read from the sensor and then send the data to The 7-Segment */
{
	switch(Desired_Temp_1) 																	/* The Data Is Expected to be from 18 ~ 35 for the Piltiers and 1~3 for The Fans */
	{
		case 0: (PORTC = SEVEN_SEG_ZERO								);		/* for 20 and 30 */
		break;
		case 1: (PORTC = SEVEN_SEG_ONE								);		/* For The Fan and 10s*/
			break;
		case 2: (PORTC = SEVEN_SEG_TWO								);		/* For The Fan and 20s */
			break;
		case 3: (PORTC = SEVEN_SEG_THREE							);		/* For The Fan and 30s */
			break;
		case 4: (PORTC = SEVEN_SEG_FOUR								);		/* The rest are for the units and Piltiers */
			break;
		case 5: (PORTC = SEVEN_SEG_FIVE								);
			break;
		case 6: (PORTC = SEVEN_SEG_SIX								);
			break;
		case 7: (PORTC = SEVEN_SEG_SEVEN							);
			break;
		case 8: (PORTC = SEVEN_SEG_EIGHT							);
			break;
		case 9: (PORTC = SEVEN_SEG_NINE								);
			break;
		case 18: (PORTC = SEVEN_SEG_EIGHTEEN					);
			break;
		case 19: (PORTC = SEVEN_SEG_NINETEEN					);
			break;
		case 20: (PORTC = SEVEN_SEG_TWENTY						);
			break;
		case 21: (PORTC = SEVEN_SEG_TWENTY_ONE				);
			break;
		case 22: (PORTC = SEVEN_SEG_TWENTY_TWO				);
			break;
		case 23: (PORTC = SEVEN_SEG_TWENTY_THREE			);
			break;
		case 24: (PORTC = SEVEN_SEG_TWENTY_FOUR				);
			break;
		case 25: (PORTC = SEVEN_SEG_TWENTY_FIVE				);
			break;
		case 26: (PORTC = SEVEN_SEG_TWENTY_SIX				);
			break;
		case 27: (PORTC = SEVEN_SEG_TWENTY_SEVEN			);
			break;
		case 28: (PORTC = SEVEN_SEG_TWENTY_EIGHT			);
			break;
		case 29: (PORTC = SEVEN_SEG_TWENTY_NINE				);
			break;
		case 30: (PORTC = SEVEN_SEG_THIRTY						);
			break;
		case 31: (PORTC = SEVEN_SEG_THIRTY_ONE				);
			break;
		case 32: (PORTC = SEVEN_SEG_THIRTY_TWO				);
			break;
		case 33: (PORTC = SEVEN_SEG_THIRTY_THREE			);
			break;
		case 34: (PORTC = SEVEN_SEG_THIRTY_FOUR			);
			break;
		case 35: (PORTC = SEVEN_SEG_THIRTY_FIVE			);
			break;
		default:
			break;
	}
}

void Seven_Seg_Fan_Write(uint16_t Speed_Level_1)
																							/* This data should be the readings from the Temp Sensor */
																							/* You have to read from the sensor and the send the data to The 7-Segment */
{
	switch(Speed_Level_1) /* The Data Is Expected to be from 18 ~ 35 for the Piltiers and 1~3 for The Fans */
	{
		case 1: (PORTA = SEVEN_SEG_ONE				);		/* For The Fan */
			break;
		case 2: (PORTA = SEVEN_SEG_TWO				);		/* For The Fan */
			break;
		case 3: (PORTA = SEVEN_SEG_THREE			);		/* For The Fan */
			break;
		default:
			break;
	}
}
