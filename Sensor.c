#include <stdio.h>
#define F_CPU 8000000UL
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <avr/io.h>																								                         	/* Main Input/Output Header */
#include <util/delay.h>																								                      /* Delay Header */
#include <avr/interrupt.h>
#include "MACROS.h"
#include "DIO.h"
#include "Sensor.h"


void VSensor_Init()
{
	DIO_VSetPinDir( 'D' , DHT11_PIN , 'H' );
	DIO_VSetPort_IO( 'D' , DHT11_PIN , 'H' );

}
void Request()					/* Microcontroller send start pulse or request */
{
	DDRD |= (HIGH<<DHT11_PIN);
	PORTD &= ~(HIGH<<DHT11_PIN);	/* set to low pin */
	_delay_ms(20);				/* wait for 20ms */
	PORTD |= (HIGH<<DHT11_PIN);	/* set to high pin */
}

void Response()					/* receive response from DHT11 */
{
	DDRD &= ~(HIGH<<DHT11_PIN);
	while(PIND & (HIGH<<DHT11_PIN));
	while((PIND & (HIGH<<DHT11_PIN))==0);
	while(PIND & (HIGH<<DHT11_PIN));
}

uint8_t Receive_data()			/* receive data */
{
	uint8_t c=0;
	int Bit_Counter;
	for ( Bit_Counter=0; Bit_Counter<8; Bit_Counter++)
	{
		while((PIND & (HIGH<<DHT11_PIN)) == LOW);	/* check received bit 0 or 1 */
		_delay_us(30);
		if(PIND & (HIGH<<DHT11_PIN))	 /* if high pulse is greater than 30ms */
		{
		c = (c<<HIGH)|(0x01);		     /* then its logic HIGH */
		}
		else				         /* otherwise its logic LOW */
		{
			c = (c<<HIGH);
		}
		while(PIND & (HIGH<<DHT11_PIN));
	}
	return c;
}




//
//Start Pulse
//------------->first is to send request to DHT11 sensor,
//pull down the data pin minimum 18ms and then pull up,
//
//void Temp_Sensor_Start_Pulse()  /* This function tells the Sensor To Send The Readings */
//{
//  DATA_PIN = 0;     /* Pull The Data Pin Down */
//  _delay_ms(20);    /* Delay for at least 18ms */
//  DATA_PIN = 1;     /* Pull The Data PIN UP again */
//}

//------------->Second, sensor will send response pulse and
//>>The response pulse is low for 54us and then goes high for 80us.
//void Temp_sensor_Wait_Response()
//{
//while(DATA_PIN == 1)      /* Wait Until The Data PIN is pull down which indicates The sensor sent a response */
//{
//  ;                       /* Do Nothing */
//}
//_delay_ms(10);            /* Delay 10 ms to make sure the Response Pulse Is over */
//}
//------------->then it starts sending data of total 40 bits to the microcontroller.
//>>After sending the response pulse, DHT11 sensor sends the data, which contains humidity
//and temperature value along with checksum. The data frame is of total 40 bits long,
// it contains 5 segments (byte) and each segment is 8-bit long.In this 5 segments first
// two segments content humidity value in decimal integer form. This Value gives us Relative
// Percentage Humidity. 1st 8-bits are integer part and next 8 bits are fractional partNext
// two segment content temperature value in decimal integer form. This value gives us
// temperature in Celsius form.
//uint64_t Sensor_Values=0;       /* 64 bit to read the 40 bit of the sensor */
//uint16_t Temp_Value = (Sensor_Values<<16);  /* Neglect The Humidity and Checksum and Read The Tempreture Value*/
//uint8_t Checksum_Check = (( (uint8_t *(Sensor_Values & 0xFFFF000000000000)) + (uint8_t *(Sensor_Values & 0x0000FFFF00000000))) && (Sensor_Values & 0x00000000FF0000)); /* Sum The Humidity and Temp then Compare it to the Checksum */
///* if the Checksum is TRUE (1) then no errors
//>>Last segment is the check sum which holds check sum of first four segment.Here checksum byte is direct addition of humidity and temperature value.
//End-Pulse
//After sending 40-bit data, DHT11 sensor sends 54us low level and then goes high. After this DHT11 goes in sleep mode.
