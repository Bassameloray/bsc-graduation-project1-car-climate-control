#include <stdio.h>
#define F_CPU 8000000UL
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <avr/io.h>																								                         	/* Main Input/Output Header */
#include <util/delay.h>																								                      /* Delay Header */
#include <avr/interrupt.h>
#include "MACROS.h"
#include "DIO.h"
#include "Piltiers.h"

void Piltiers_Init()
{
  DIO_VSetPinDir( 'D' , PILTIERS_PIN , 'H');
  DIO_VSetPort_IO( 'D' , PILTIERS_PIN ,'H');      /* Initialize The PORT as output */
}

void Piltiers_Control(uint8_t Piltiers_Status_2)    /* The Function is meant to cntrol the Piltiers */
{
  switch(Piltiers_Status_2) /* Either On Or Off */
  {
    case 1:
    {
      DIO_VSetPort_IO( 'D' , PILTIERS_PIN ,'H');    /* Turn On The Piltiers By Making The Port Output */
      break;
    }
    case 0:
    {
      DIO_VSetPort_IO( 'D' , PILTIERS_PIN ,'L');    /* Turn Off The Piltiers By Making The Port Input */
      break;
    }
    default:
      break;
  }
}
